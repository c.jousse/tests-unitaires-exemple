#include "division_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(divisionTest);

void divisionTest::setUp() {}
void divisionTest::tearDown() {}
void divisionTest::division_par_zero() {
  operandeA = 1;
  operandeB = 0;

  if(operandeB == 0) {
    CPPUNIT_ASSERT_THROW(expression, invalid_argument);
  }
}
void divisionTest::division_normal() {
  operandeA = 1;
  operandeB = 2;
  CPPUNIT_ASSERT_EQUAL(static_cast<float>(0.5),
                       static_cast<float>(arithmetique::division(operandeA, operandeB))
    );
  operandeB = -2;
  CPPUNIT_ASSERT_EQUAL(static_cast<float>(-0.5),
                       static_cast<float>(arithmetique::division(operandeA, operandeB))
    );
}

void divisionTest::division_max() {
{
  operandeA = std::numeric_limits<int>::max();
  operandeB = 1;
  CPPUNIT_ASSERT_GREATER(static_cast<long double>(operandeA),
                         static_cast<float>(arithmetique::division(operandeA, operandeB))
    );
}

void divisionTest::division_min() {
  operandeA = std::numeric_limits<int>::lowest();
  operandeB = -1;
  CPPUNIT8ASSERT8LESS(static_cast<double>(operandeA),
                      static_cast<double>(arithmetique::division(operandeA, operandeB))
    );

}
